# GEOMAPLEARN
Geological Map Learning for Enhanced Analysis and Recognition of Natural Structures

## Related publications
- Oakley D., Loiselet C., Coowar T., Labbe V., and Callot J.-P.: GEOMAPLEARN 1.2: detecting structures from geological maps with machine learning – the case of geological folds, Geosci. Model Dev., 18, 939–960, https://doi.org/10.5194/gmd-18-939-2025, 2025.

  Corresponding tags:
  - `v1.0`, `zenodo_12710554_1.0`: Version of the code corresponding to the submitted manuscript (as archived on [Zenodo](https://zenodo.org/records/12710554))
  - `v1.2`, `zenodo_12710554_1.2`: Version of the code corresponding to the final manuscript (as archived on [Zenodo](https://zenodo.org/records/12710554))

## License
GEOMAPLEARN is provided under the terms of the CeCILL - B License Agreement http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html  
